php-mongodb-1-9 (1:1.9.2-1) unstable; urgency=medium

  * New upstream version 1.9.2
  * Split the source package for PHP 7.0

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 10:01:55 +0200

php-mongodb (1.15.0+1.11.1+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.15.0+1.11.1+1.9.2+1.7.5
  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 13:54:38 +0100

php-mongodb (1.14.0+1.11.1+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.14.0+1.11.1+1.9.2+1.7.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 25 Aug 2022 12:39:20 +0200

php-mongodb (1.13.0+1.11.1+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.13.0+1.11.1+1.9.2+1.7.5
   + mongodb 1.7.5 is used for PHP 5.6
   + mongodb 1.9.2 is used for PHP 7.0
   + mongodb 1.11.1 is used for PHP 7.1
   + mongodb 1.13.0 is used for PHP 7.2-8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 25 Jun 2022 09:41:16 +0200

php-mongodb (1.13.0+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.13.0+1.9.2+1.7.5

 -- Ondřej Surý <ondrej@debian.org>  Thu, 21 Apr 2022 12:44:17 +0200

php-mongodb (1.12.0+1.9.2+1.7.5-4) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:28 +0100

php-mongodb (1.12.0+1.9.2+1.7.5-3) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:51 +0100

php-mongodb (1.12.0+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.12.0+1.9.2+1.7.5

 -- Ondřej Surý <ondrej@debian.org>  Fri, 31 Dec 2021 09:32:54 +0100

php-mongodb (1.11.1+1.9.2+1.7.5-4) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:25:56 +0100

php-mongodb (1.11.1+1.9.2+1.7.5-3) unstable; urgency=medium

  * Remove unused libpcre3-dev Build-Dependency (Closes: #999997)

 -- Ondřej Surý <ondrej@debian.org>  Thu, 18 Nov 2021 13:27:02 +0100

php-mongodb (1.11.1+1.9.2+1.7.5-2) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:00:59 +0100

php-mongodb (1.11.1+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.11.1+1.9.2+1.7.5
   + mongodb 1.7.5 is used for PHP 5.6
   + mongodb 1.9.2 is used for PHP 7.0
   + mongodb 1.11.1 is used for PHP 7.1-8.1

 -- Ondřej Surý <ondrej@debian.org>  Tue, 09 Nov 2021 12:21:57 +0100

php-mongodb (1.11.0+1.9.2+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.11.0+1.9.2+1.7.5
   + mongodb 1.7.5 is used for PHP 5.6
   + mongodb 1.9.2 is used for PHP 7.0
   + mongodb 1.11.0 is used for PHP 7.1-8.1

 -- Ondřej Surý <ondrej@debian.org>  Tue, 02 Nov 2021 11:57:29 +0100

php-mongodb (1.11.0+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.11.0+1.7.5 (Closes: #998249)
  * Add libsnappy-dev, libzstd-dev and zlib1g-dev to enable compression in
    libmongoc (GH #1670)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 02 Nov 2021 10:55:22 +0100

php-mongodb (1.10.0+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.10.0+1.7.5

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Aug 2021 09:18:21 +0200

php-mongodb (1.9.0+1.7.5-6) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:07:20 +0100

php-mongodb (1.9.0+1.7.5-5) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:37 +0100

php-mongodb (1.9.0+1.7.5-4) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:50:58 +0100

php-mongodb (1.9.0+1.7.5-3) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:52 +0100

php-mongodb (1.9.0+1.7.5-2) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:40:23 +0100

php-mongodb (1.9.0+1.7.5-1) unstable; urgency=medium

  * New upstream version 1.9.0+1.7.5
   + Restore PHP 5.6 support via mongodb 1.7.5

 -- Ondřej Surý <ondrej@debian.org>  Wed, 23 Dec 2020 17:55:30 +0100

php-mongodb (1.9.0-1) unstable; urgency=medium

  * New upstream version 1.9.0
   + Adds PHP 8.0 support

 -- Ondřej Surý <ondrej@debian.org>  Mon, 07 Dec 2020 12:43:39 +0100

php-mongodb (1.8.1-2) unstable; urgency=medium

  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 14:38:34 +0200

php-mongodb (1.8.1-1) unstable; urgency=medium

  * New upstream version 1.8.1
  * Build with dh-php >= 1.0

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Oct 2020 22:05:24 +0200

php-mongodb (1.7.4-1) unstable; urgency=medium

  * New upstream version 1.7.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Mar 2020 11:27:31 +0100

php-mongodb (1.7.3-1) unstable; urgency=medium

  * New upstream version 1.7.3

 -- Ondřej Surý <ondrej@debian.org>  Wed, 26 Feb 2020 21:42:31 +0100

php-mongodb (1.6.1-4) unstable; urgency=medium

  * Disable the tests properly with dh-php >= 0.35

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 13:17:15 +0100

php-mongodb (1.6.1-3) unstable; urgency=medium

  * Really disable the tests

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 13:03:58 +0100

php-mongodb (1.6.1-2) unstable; urgency=medium

  * Disable running tests, as they requires json extension while disabling
    all extensions at the test time

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:52:35 +0100

php-mongodb (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 12:03:27 +0100

php-mongodb (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0

 -- Ondřej Surý <ondrej@debian.org>  Sun, 17 Nov 2019 03:11:46 +0100

php-mongodb (1.5.5-1) unstable; urgency=medium

  * Add Pre-Depends on php-common >= 0.69~
  * New upstream version 1.5.5

 -- Ondřej Surý <ondrej@sury.org>  Thu, 08 Aug 2019 08:49:06 +0200

php-mongodb (1.5.3-2) unstable; urgency=medium

  * Bump the dependency on dh-php to >= 0.33

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 10:19:35 +0000

php-mongodb (1.5.3-1) unstable; urgency=medium

  * New upstream version 1.5.3

 -- Ondřej Surý <ondrej@debian.org>  Wed, 03 Oct 2018 06:58:47 +0000

php-mongodb (1.5.2-1) unstable; urgency=medium

  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899645)
  * New upstream version 1.5.2

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 12:47:08 +0000

php-mongodb (1.4.2-1) unstable; urgency=medium

  * New upstream version 1.4.2

 -- Ondřej Surý <ondrej@debian.org>  Tue, 13 Mar 2018 13:03:09 +0000

php-mongodb (1.3.4-1) unstable; urgency=medium

  * New upstream version 1.3.4

 -- Ondřej Surý <ondrej@debian.org>  Thu, 01 Feb 2018 15:07:41 +0000

php-mongodb (1.3.3-1) unstable; urgency=medium

  * New upstream version 1.3.3

 -- Ondřej Surý <ondrej@debian.org>  Fri, 01 Dec 2017 16:16:00 +0000

php-mongodb (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Sep 2017 18:59:08 +0200

php-mongodb (1.2.10-1) unstable; urgency=medium

  * New upstream version 1.2.10

 -- Ondřej Surý <ondrej@debian.org>  Thu, 07 Sep 2017 20:28:34 +0200

php-mongodb (1.2.9-1) unstable; urgency=medium

  * New upstream version 1.2.9

 -- Ondřej Surý <ondrej@debian.org>  Tue, 30 May 2017 10:06:21 +0200

php-mongodb (1.2.8-1) unstable; urgency=medium

  * New upstream version 1.2.8

 -- Ondřej Surý <ondrej@debian.org>  Thu, 27 Apr 2017 00:08:43 +0200

php-mongodb (1.2.3-1) unstable; urgency=medium

  * New upstream version 1.2.3

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Jan 2017 11:37:22 +0100

php-mongodb (1.2.0-1) unstable; urgency=medium

  * Imported Upstream version 1.2.0

 -- Ondřej Surý <ondrej@debian.org>  Thu, 01 Dec 2016 11:18:34 +0100

php-mongodb (1.1.9-1) unstable; urgency=medium

  * Imported Upstream version 1.1.9

 -- Ondřej Surý <ondrej@debian.org>  Fri, 21 Oct 2016 13:11:11 +0200

php-mongodb (1.1.7-2) unstable; urgency=medium

  * Fix DEB_MAINT_CFLAGS_APPEND

 -- Ondřej Surý <ondrej@debian.org>  Mon, 13 Jun 2016 14:08:24 +0200

php-mongodb (1.1.7-1) unstable; urgency=medium

  * Append -DMONGOC_NO_AUTOMATIC_GLOBALS to CFLAGS
  * Imported Upstream version 1.1.7

 -- Ondřej Surý <ondrej@debian.org>  Mon, 13 Jun 2016 14:02:38 +0200

php-mongodb (1.1.6-3) unstable; urgency=medium

  * Add ${php:Provides} to d/control and reformat with wrap-and-sort -a
  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 11:18:13 +0200

php-mongodb (1.1.6-2) unstable; urgency=medium

  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 15:56:13 +0200

php-mongodb (1.1.6-1) unstable; urgency=medium

  * Imported Upstream version 1.1.6
  * Update d/copyright will all the mess introduced by embedding libbson
    and libmongoc
  * Bump d/compat to level 10
  * Bump policy to 3.9.8

 -- Ondřej Surý <ondrej@debian.org>  Wed, 20 Apr 2016 14:12:29 +0200

php-mongodb (1.1.5-1) unstable; urgency=medium

  * Imported Upstream version 1.1.5
  * Make sure system libbson-dev and libmongoc-dev is not installed via
    Build-Conflicts
  * Don't use system libbson and libmongoc until the upstream linking with
    priv symbols is resolved

 -- Ondřej Surý <ondrej@debian.org>  Sat, 19 Mar 2016 07:42:28 +0100

php-mongodb (1.1.4-1) unstable; urgency=medium

  * Imported Upstream version 1.1.4
  * Rebase patch on top of 1.1.4 release and update libmongoc-priv to
    libmongoc 1.3.3 files
  * Add hard dependency on libmongoc and libbson >= 1.3.3
  * For time being, just use bundled libmongoc library and hope it will
    get resolved for Debian stretch release

 -- Ondřej Surý <ondrej@debian.org>  Mon, 14 Mar 2016 20:49:13 +0100

php-mongodb (1.1.2-3) unstable; urgency=medium

  * Add pkg-config to Build-Depends
  * Force rebuild with dh_php >= 0.7

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Feb 2016 22:26:11 +0100

php-mongodb (1.1.2-2) unstable; urgency=medium

  * Build-Depend on libbson and libmongoc >= 1.3.0-1~
  * Skip compilation against libmongoc-priv-dev and use the copies of
    needed private parts of the library

 -- Ondřej Surý <ondrej@debian.org>  Mon, 18 Jan 2016 13:25:47 +0100

php-mongodb (1.1.2-1) unstable; urgency=medium

  * Fix copy&paste error pretending this to be zend_extension
  * Imported Upstream version 1.1.2
  * Remove 32-bits patch; merged upstream
  * Use system libbson and libmongoc for building the extension

 -- Ondřej Surý <ondrej@debian.org>  Thu, 14 Jan 2016 14:23:11 +0100

php-mongodb (1.1.1-2) unstable; urgency=medium

  * Fix PHP 7 / 32bits build (Courtesy of Remi Collet)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 29 Dec 2015 17:58:45 +0100

php-mongodb (1.1.1-1) unstable; urgency=medium

  * Don't use system libbson and libmongoc as libmongoc private headers
    are used in the PHP driver (ugly and needs to be fixed)
  * Initial release (Closes: #809326)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 29 Dec 2015 12:56:50 +0100
